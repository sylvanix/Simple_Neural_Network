'''
this is a modified version of the code found here:
https://dev.to/shamdasani/build-a-flexible-neural-network-with-backpropagation-in-python
'''

import numpy as np

# X = (hours sleeping, hours studying), y = score on test
X = np.array(([1,1,1],[1,0,0],[0,1,0],[0,0,1],[1,1,0], [0,1,1]), dtype=float)
y = np.array(([0], [1], [1], [0], [0], [1]), dtype=float)
lr = 0.01

class Neural_Network(object):
    def __init__(self):
        #parameters
        self.inputSize = 3
        self.outputSize = 1
        self.hiddenSize = 5

        #weights
        self.W1 = np.random.randn(self.inputSize, self.hiddenSize) # (3x2) weight matrix from input to hidden layer
        self.W2 = np.random.randn(self.hiddenSize, self.outputSize) # (3x1) weight matrix from hidden to output layer

    def forward(self, X):
        #forward propagation through our network
        self.z = np.dot(X, self.W1) # dot product of X (input) and first set of 3x2 weights
        self.z2 = self.sigmoid(self.z) # activation function
        self.z3 = np.dot(self.z2, self.W2) # dot product of hidden layer (z2) and second set of 3x1 weights
        o = self.sigmoid(self.z3) # final activation function
        return o 

    def sigmoid(self, s):
        # activation function 
        return 1/(1+np.exp(-s))

    def sigmoidPrime(self, s):
        #derivative of sigmoid
        return s * (1 - s)

    def backward(self, X, y, o):
        # backward propgate through the network
        self.o_error = y - o # error in output
        self.o_delta = self.o_error*self.sigmoidPrime(o) # applying derivative of sigmoid to error

        self.z2_error = self.o_delta.dot(self.W2.T) # z2 error: how much our hidden layer weights contributed to output error
        self.z2_delta = self.z2_error*self.sigmoidPrime(self.z2) # applying derivative of sigmoid to z2 error

        self.W1 += lr * X.T.dot(self.z2_delta) # adjusting first set (input --> hidden) weights
        self.W2 += lr * self.z2.T.dot(self.o_delta) # adjusting second set (hidden --> output) weights

    def train (self, X, y):
        o = self.forward(X)
        self.backward(X, y, o)


NN = Neural_Network()
for i in range(100000):
    if i % 10000 == 0: print( "Loss: " + str(np.mean(np.square(y - NN.forward(X)))) ) # mean sum squared loss
    NN.train(X, y)

print( "\nPredicted output:" )
for x in NN.forward(X):
    print('    ', x) 

